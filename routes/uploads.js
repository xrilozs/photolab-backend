const express = require('express')
const router = express.Router()
const UploadController = require('../controllers/UploadController')
const {authenticateToken, authenticateRefreshToken} = require('../middlewares/auth')

router.post("/upload/login", UploadController.login)
router.get("/upload/refresh", authenticateRefreshToken, UploadController.refreshToken)
router.get("/upload/step1orderStatus", authenticateToken, UploadController.step1orderStatus)
router.get("/upload/step2order", authenticateToken, UploadController.step2order)
router.get("/upload/step2countOrder", authenticateToken, UploadController.step2countOrder)
router.get("/upload/attribute", authenticateToken, UploadController.getAttribute)
router.put("/upload/titleTemplate", authenticateToken, UploadController.editTitleTemplate)
router.get("/upload/productNamePage", authenticateToken, UploadController.getProductNamePage)
router.get("/upload/productThemeDetail", authenticateToken, UploadController.getProductThemeDetail)
router.get("/upload/productThemeName", authenticateToken, UploadController.getProductThemeName)
router.post("/upload/uploadImage", authenticateToken, UploadController.uploadImage)
router.post("/upload/deleteImage", authenticateToken, UploadController.deleteImage)

module.exports = router