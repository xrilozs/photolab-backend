const LoginProcedure = require('../models/LoginProcedure')
const Step2OrderProcedure = require('../models/Step2OrderProcedure')
const Step2OrderPStep1OrderStatusProcedurerocedure = require('../models/Step1OrderStatusProcedure')
const Step2CountOrderProcedure = require('../models/Step2CountOrderProcedure')
const AttributeProcedure = require('../models/AttributeProcedure')
const { EditTitleTemplateProcedure } = require('../models/TitleTemplateProcedure')
const ProductNamePageProcedure = require('../models/ProductNamePageProcedure')
const ProducThemeDetailProcedure = require('../models/ProducThemeDetailProcedure')
const ProducThemeNameProcedure = require('../models/ProducThemeNameProcedure')
const InsertImageProcedure = require('../models/InsertImageProcedure')
const DeleteImageProcedure = require('../models/DeleteImageProcedure')
const IsAggreePolicyProcedure = require('../models/IsAggreePolicyProcedure')

async function getOrderByIDAndPhoneNo(orderId, phoneNo){
    return await LoginProcedure(orderId, phoneNo)
}

async function getOrderByID(orderId){
    return await Step2OrderProcedure(orderId)
}

async function getOrderStatusByID(orderId){
    return await Step2OrderPStep1OrderStatusProcedurerocedure(orderId)
}

async function countOrderByID(orderId){
    return await Step2CountOrderProcedure(orderId)
}

async function getAttribute(){
    return await AttributeProcedure()
}

async function editTitleTemplate(idOrder, idProduct, sort, title){
    return await EditTitleTemplateProcedure(idOrder, idProduct, sort, title)
}

async function getProductNamePage(idProduct){
    return await ProductNamePageProcedure(idProduct)
}

async function getProductThemeDetail(idProduct){
    return await ProducThemeDetailProcedure(idProduct)
}

async function getProductThemeName(idProduct){
    return await ProducThemeNameProcedure(idProduct)
}

async function insertImage(idOrder, idProduct, sort, title, photourl, tumburl){
    return await InsertImageProcedure(idOrder, idProduct, sort, title, photourl, tumburl)
}

async function deleteImage(idOrder, idProduct, sort, title){
    return await DeleteImageProcedure(idOrder, idProduct, sort, title)
}

async function isAggreePolicy(idOrder, idproduct, isPolicyAgree, PolicyAgreeID, PolicyAgreeDevice, PolicyAgreeIp){
    return await IsAggreePolicyProcedure(idOrder, idproduct, isPolicyAgree, PolicyAgreeID, PolicyAgreeDevice, PolicyAgreeIp)
}

module.exports = {
    getOrderByIDAndPhoneNo,
    getOrderByID,
    getOrderStatusByID,
    countOrderByID,
    getAttribute,
    editTitleTemplate,
    getProductNamePage,
    getProductThemeDetail,
    getProductThemeName,
    insertImage,
    deleteImage,
    isAggreePolicy
}