const jwt = require('jsonwebtoken');
const dotenv = require('dotenv')
dotenv.config({path: `.env.${process.env.NODE_ENV}`})

function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]

  if (token == null) return res.sendStatus(401)

  jwt.verify(token, process.env.TOKEN_SECRET, (err, order) => {
    console.log(err)

    if (err) return res.sendStatus(403)

    req.order = order

    next()
  })
}

function authenticateRefreshToken(req, res, next) {
  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]

  if (token == null) return res.sendStatus(401)

  jwt.verify(token, process.env.REFRESH_SECRET, (err, order) => {
    console.log(err)

    if (err) return res.sendStatus(403)

    req.order = order

    next()
  })
}

module.exports = {
  authenticateToken,
  authenticateRefreshToken
}