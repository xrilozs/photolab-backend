// const customMid = require('./middlewares/customMid')
const uploadRoutes = require('./routes/uploads')

const morgan = require('morgan')
const express = require('express')
const path = require('path');
const dotenv = require('dotenv')
dotenv.config({path: path.resolve(__dirname, `.env.${process.env.NODE_ENV}`) })

const app = express()

app.use(morgan('dev'))
app.use(express.json())
app.use(uploadRoutes)

app.get("/", function(req, res){
    res.send(JSON.stringify({
        message: "Welcome to Express"
    }))
})

app.listen(3000, function(){
    console.log("Express server running in port 3000!")
})