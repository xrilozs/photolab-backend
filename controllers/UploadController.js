const UploadsRepository = require('../repositories/UploadsRepository')
const Response = require("../models/BasicResponse")
const {checkParamByKeys, isNullOrUndefined} = require("../helpers/common")
const {encryptPassword, verifyPassword, generateAccessToken, generateRefreshToken} = require("../helpers/encrypt")
const mail = require('../helpers/mail')
const file = require('../helpers/file')
const { parseFormData, uploadFile } = require('../helpers/form')

async function login(req, res){
    const body = req.body
    if(!checkParamByKeys(body, ['orderId', 'phoneNo'])){
        const response = new Response(400, "Invalid request body", null)
        return response.setResponse(res)
    }
    let { orderId, phoneNo } = body

    const order = await UploadsRepository.getOrderByIDAndPhoneNo(orderId, phoneNo)
    if(isNullOrUndefined(order)){
        const response = new Response(404, "Order not found", null)
        return response.setResponse(res)
    }
    if(order.result != 'Valid'){
        const response = new Response(400, "Invalid Order ID and Phone No!", null)
        return response.setResponse(res)
    }

    const token = {
        accessToken: generateAccessToken({orderId, phoneNo}),
        refreshToken: generateRefreshToken({orderId, phoneNo}),
        order
    }

    const response = new Response(200, "login order success!", token)
    return response.setResponse(res)
}

async function refreshToken(req, res){
    if(!checkParamByKeys(req, ['order'])){
        const response = new Response(400, "Invalid request", null)
        return response.setResponse(res)
    }

    const reqOrder = req.order
    const order = await UploadsRepository.getOrderByIDAndPhoneNo(reqOrder.orderId, reqOrder.phoneNo)
    if(isNullOrUndefined(order)){
        const response = new Response(404, "Order not found", null)
        return response.setResponse(res)
    }
    if(order.result != 'Valid'){
        const response = new Response(400, "Invalid Order ID and Phone No!", null)
        return response.setResponse(res)
    }
    let {orderId, phoneNo} = reqOrder

    const token = {
        accessToken: generateAccessToken({orderId, phoneNo}),
        refreshToken: generateRefreshToken({orderId, phoneNo})
    }

    const response = new Response(200, "refresh token success!", token)
    return response.setResponse(res)
}

async function step2order(req, res){
    if(!checkParamByKeys(req, ['order'])){
        const response = new Response(400, "Invalid request body", null)
        return response.setResponse(res)
    }
    let { orderId } = req.order

    const order = await UploadsRepository.getOrderByID(orderId)
    if(isNullOrUndefined(order)){
        const response = new Response(404, "Order not found", null)
        return response.setResponse(res)
    }

    const response = new Response(200, "get order success!", order)
    return response.setResponse(res)
}

async function step1orderStatus(req, res){
    if(!checkParamByKeys(req, ['order'])){
        const response = new Response(400, "Invalid request body", null)
        return response.setResponse(res)
    }
    let { orderId } = req.order

    const order = await UploadsRepository.getOrderStatusByID(orderId)
    if(isNullOrUndefined(order)){
        const response = new Response(404, "Order not found", null)
        return response.setResponse(res)
    }

    const response = new Response(200, "get order status success!", order)
    return response.setResponse(res)
}

async function step2countOrder(req, res){
    if(!checkParamByKeys(req, ['order'])){
        const response = new Response(400, "Invalid request body", null)
        return response.setResponse(res)
    }
    let { orderId } = req.order

    const order = await UploadsRepository.countOrderByID(orderId)
    if(isNullOrUndefined(order)){
        const response = new Response(404, "Order not found", null)
        return response.setResponse(res)
    }

    const response = new Response(200, "count order success!", order)
    return response.setResponse(res)
}

async function getAttribute(req, res){
    const attributes = await UploadsRepository.getAttribute()
    if(isNullOrUndefined(attributes)){
        const response = new Response(404, "Attribute not found", null)
        return response.setResponse(res)
    }

    const response = new Response(200, "Get attribute success!", attributes)
    return response.setResponse(res)
}

async function editTitleTemplate(req, res){
    const body = req.body
    const order = req.order
    if(!checkParamByKeys(body, ['idProduct', 'sort', 'title'])){
        const response = new Response(400, "Invalid request body", null)
        return response.setResponse(res)
    }
    let { idProduct, sort, title } = body
    let { orderId } = order

    const isUpadated = await UploadsRepository.editTitleTemplate(orderId, idProduct, sort, title)

    if(!isUpadated){
        const response = new Response(400, "Edit title template failed", null)
        return response.setResponse(res)
    }

    const response = new Response(200, "login order success!", isUpadated)
    return response.setResponse(res)
}

async function getProductNamePage(req, res){
    const query = req.query
    if(!checkParamByKeys(query, ['idProduct'])){
        const response = new Response(400, "Invalid Query Param", null)
        return response.setResponse(res)
    }
    let { idProduct } = query

    const productNamePage = await UploadsRepository.getProductNamePage(idProduct)

    if(isNullOrUndefined(productNamePage)){
        const response = new Response(404, "Product name page not found", null)
        return response.setResponse(res)
    }

    const response = new Response(200, "Get product name page success!", productNamePage)
    return response.setResponse(res)
}

async function getProductThemeDetail(req, res){
    const query = req.query
    if(!checkParamByKeys(query, ['idProduct'])){
        const response = new Response(400, "Invalid Query Param", null)
        return response.setResponse(res)
    }
    let { idProduct } = query

    const productThemeDetail = await UploadsRepository.getProductThemeDetail(idProduct)

    if(isNullOrUndefined(productThemeDetail)){
        const response = new Response(404, "Product theme detail not found", null)
        return response.setResponse(res)
    }

    const response = new Response(200, "Get product theme detail success!", productThemeDetail)
    return response.setResponse(res)
}

async function getProductThemeName(req, res){
    const query = req.query
    if(!checkParamByKeys(query, ['idProduct'])){
        const response = new Response(400, "Invalid Query Param", null)
        return response.setResponse(res)
    }
    let { idProduct } = query

    const productThemeName = await UploadsRepository.getProductThemeName(idProduct)

    if(isNullOrUndefined(productThemeName)){
        const response = new Response(404, "Product theme name not found", null)
        return response.setResponse(res)
    }

    const response = new Response(200, "Get product theme name success!", productThemeName)
    return response.setResponse(res)
}

async function uploadImage(req, res){
    const order = req.order
    let { fields, files } = await parseFormData(req)
    console.log("FIELDS: ", fields)
    if(!checkParamByKeys(fields, ['idProduct', 'sort', 'title'])){
        const response = new Response(400, "Invalid form data", null)
        return response.setResponse(res)
    }

    let { idProduct, sort, title } = fields
    let { orderId } = order

    try{
        let uploadResp = await file.uploadFile(files)
        console.log("UPLOAD RESP", uploadResp)

        if("path" in uploadResp){
            console.log("upload success")
            const insertResp = await UploadsRepository.insertImage(orderId, idProduct, parseInt(sort), title, uploadResp.path, uploadResp.thumbnailpath)
            const response = new Response(200, "Upload image success", insertResp)
            return response.setResponse(res)
        }else{
            const response = new Response(500, "Upload image failed", uploadResp)
            return response.setResponse(res)
        }
    }catch(error){
        console.log(error)
        const response = new Response(500, "Upload image failed", error)
        return response.setResponse(res)
    }
}

async function deleteImage(req, res){
    const order = req.order
    const body = req.body
    console.log("body: ", body)
    if(!checkParamByKeys(body, ['idProduct', 'sort', 'title'])){
        const response = new Response(400, "Invalid payload data", null)
        return response.setResponse(res)
    }

    let { idProduct, sort, title } = body
    let { orderId } = order

    const deleteResp = await UploadsRepository.deleteImage(orderId, idProduct, parseInt(sort), title)
    if(isNullOrUndefined(deleteResp)){
        const response = new Response(500, "Delete image failed")
        return response.setResponse(res)
    }

    const response = new Response(200, "Delete image success", deleteResp)
    return response.setResponse(res)
}

async function isAggreePolicy(req, res){
    const order = req.order
    const body = req.body
    console.log("body: ", body)
    if(!checkParamByKeys(body, ['idProduct', 'isPolicyAgree', 'PolicyAgreeID'])){
        const response = new Response(400, "Invalid payload data", null)
        return response.setResponse(res)
    }

    let { idProduct, isPolicyAgree, PolicyAgreeID } = body
    let { orderId } = order

    const ipAddress = req.ip;
    const hostname = req.hostname;

    const isAggreePolicy = await UploadsRepository.isAggreePolicy(orderId, idProduct, isPolicyAgree, PolicyAgreeID, hostname, ipAddress)
    if(isNullOrUndefined(isAggreePolicy)){
        const response = new Response(500, "Is Aggree Policy failed")
        return response.setResponse(res)
    }

    const response = new Response(200, "Is Aggree Policy success", isAggreePolicy)
    return response.setResponse(res)
}

module.exports = {
    login,
    refreshToken,
    step2order,
    step1orderStatus,
    step2countOrder,
    getAttribute,
    editTitleTemplate,
    getProductNamePage,
    getProductThemeDetail,
    getProductThemeName,
    uploadImage,
    deleteImage,
    isAggreePolicy
}