const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const salt = bcrypt.genSaltSync(10);
const dotenv = require('dotenv')
dotenv.config({path: `.env.${process.env.NODE_ENV}`})

function encryptPassword(plainPassword){
    const hash = bcrypt.hashSync(plainPassword, salt);
    return hash
}

function verifyPassword(plainPassword, hash){
    const isValid = bcrypt.compareSync(plainPassword, hash);
    return isValid
}

function generateAccessToken(payload) {
    return jwt.sign(payload, process.env.TOKEN_SECRET, { expiresIn: '600s' });
}

function generateRefreshToken(payload) {
    return jwt.sign(payload, process.env.REFRESH_SECRET, { expiresIn: '1h' });
}

module.exports = {
    encryptPassword,
    verifyPassword,
    generateAccessToken,
    generateRefreshToken
}