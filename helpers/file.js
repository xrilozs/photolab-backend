const path = require('path');
const mv = require('mv');
const sharp = require('sharp');

function uploadFile(files){
    return new Promise((resolve, reject) => {
        const image = files.image
        const mimeType = image.mimetype
        const mimeTypeArr = mimeType.split("/")
        const formatFile = mimeTypeArr[1]
        const oldpath = files.image.filepath;
        const newpath = path.resolve(process.cwd(), `assets/${files.image.newFilename}.${formatFile}`)
        const thumbnailpath = path.resolve(process.cwd(), `assets/${files.image.newFilename}-thumbnail.${formatFile}`)
        const fileName = `assets/${files.image.newFilename}.${formatFile}`
        const thumbnailName = `assets/${files.image.newFilename}-thumbnail.${formatFile}`

        mv(oldpath, newpath, function (err) {
            if(err) reject(err)
            else{
                sharp(newpath)
                    .resize(200)
                    .toFile(thumbnailpath, (err) => {
                    if (err) {
                        console.error(err);
                        reject(err)
                    } else {
                        resolve({
                            path: fileName,
                            thumbnailpath: thumbnailName
                        })
                    }
                });
            }
        });
    })

}

module.exports = {
    uploadFile
}