const nodemailer = require('nodemailer')
const path = require('path');
const dotenv = require('dotenv')
dotenv.config({path: path.resolve(__dirname, `.env.${process.env.NODE_ENV}`) })

function sendMail(mailTo, mailSubject, mailContent, isHtml=false){
    const transport = {
        service: process.env.MAIL_SERVICE,
        auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PASS
        }
    }
    console.log("TRANSPORT: ", transport)
    const transporter = nodemailer.createTransport(transport)

    let mailOptions = {
        from: process.env.MAIL_USER,
        to: mailTo,
        subject: mailSubject,
    }


    if(isHtml) mailOptions.text = mailContent
    else mailOptions.html = mailContent
    console.log("OPTION: ", mailOptions)

    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, function(error, info){
            if(error) reject(error)
            else resolve({info: info})
        })
    })
}

module.exports = { sendMail }