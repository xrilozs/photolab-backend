const formidable = require('formidable');
const path = require('path');
const mv = require('mv');
const sharp = require('sharp');

const formData = formidable({ multiples: true });

function parseFormData(req) {
  return new Promise((resolve, reject) => {

    formData.parse(req, (err, fields, files) => {
      if (err) {
        // Reject the promise with the error
        reject(err);
      } else {
        // Resolve the promise with the fields and files
        resolve({ fields, files });
      }
    });
  });
}

function uploadFormFile(req){

  return new Promise((resolve, reject) => {
    formData.parse(req, function (err, fields, files) {
          const image = files.image
          const mimeType = image.mimetype
          const mimeTypeArr = mimeType.split("/")
          const formatFile = mimeTypeArr[1]
          const oldpath = files.image.filepath;
          const newpath = path.resolve(process.cwd(), `assets/${files.image.newFilename}.${formatFile}`)
          const thumbnailpath = path.resolve(process.cwd(), `assets/${files.image.newFilename}-thumbnail.${formatFile}`)
  
          mv(oldpath, newpath, function (err) {
              if(err) reject(err)
              else{
                  sharp(newpath)
                      .resize(200)
                      .toFile(thumbnailpath, (err) => {
                      if (err) {
                          console.error(err);
                          reject(err)
                      } else {
                          resolve({
                              path: newpath,
                              thumbnailpath: thumbnailpath
                          })
                      }
                  });
              }
          });
      });
  })
  

}

module.exports = {
  parseFormData,
  uploadFormFile
}