function checkParams(params){
    let isValid = true
    for(let param of params){
        if(!param){
            isValid = false
            break
        }
    }
    return isValid
}

function checkParamByKeys(param, keys){
    let isValid = true
    for(let key of keys){
        if(!param[key]){
            isValid = false
            break
        }
    }
    return isValid
}

function isNullOrUndefined(data){
    if(data == null || data == undefined) return true
    else return false
}

module.exports = {
    checkParams,
    checkParamByKeys,
    isNullOrUndefined
}