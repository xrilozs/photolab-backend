const db = require('../sequelize');
const { QueryTypes, AggregateError } = require('sequelize');

async function IsAggreePolicyProcedure(idOrder, idproduct, isPolicyAgree, PolicyAgreeID, PolicyAgreeDevice, PolicyAgreeIp) {
  try{
    const result = await db.query(`
      EXEC orderprocess.s_Step2_OrderDetail_isPolicyAgree 
        @idorder            = :param1,
        @idProduct          = :param2,
        @isPolicyAgree      = :param3,
        @PolicyAgreeID      = :param4,
        @PolicyAgreeDevice  = :param5,
        @PolicyAgreeIp      = :param6,
    `, {
      replacements: { 
        param1: idOrder,
        param2: idproduct,
        param3: isPolicyAgree,
        param4: PolicyAgreeID,
        param5: PolicyAgreeDevice,
        param6: PolicyAgreeIp,
      },
      type: QueryTypes.RAW,
    });

    console.log("RESULT: ", result)

    return result[0];
  }catch(error){
    if (error instanceof AggregateError) {
      // Handle multiple errors
      const errors = error.errors;
      for (const err of errors) {
        console.error('Error:', err);
      }
    } else {
      // Handle other types of errors
      console.error('Error:', error);
    }
    
    return null;
  }
}

module.exports = IsAggreePolicyProcedure
