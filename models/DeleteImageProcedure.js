const db = require('../sequelize');
const { QueryTypes, AggregateError } = require('sequelize');

async function DeleteImageProcedure(idOrder, idproduct, sort, title) {
  try{
    const result = await db.query(`
      EXEC orderprocess.s_step2_OrderDetail_ImageUpload_delete 
        @idorder    = :param1,
        @idproduct  = :param2,
        @sort       = :param3,
        @title      = :param4
    `, {
      replacements: { 
        param1: idOrder,
        param2: idproduct,
        param3: sort,
        param4: title
      },
      type: QueryTypes.RAW,
    });

    console.log("RESULT: ", result)

    return result[0];
  }catch(error){
    if (error instanceof AggregateError) {
      // Handle multiple errors
      const errors = error.errors;
      for (const err of errors) {
        console.error('Error:', err);
      }
    } else {
      // Handle other types of errors
      console.error('Error:', error);
    }
    
    return null;
  }
}

module.exports = DeleteImageProcedure
