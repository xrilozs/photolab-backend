class Response {
    constructor(code, message, data){
        this.code = code
        this.message = message
        this.data = data
    }

    setResponse(res){
        res.status(this.code)
        res.json({
            code: this.code,
            message: this.message,
            data: this.data
        })
    }
}

module.exports = Response