const db = require('../sequelize');
const { QueryTypes, AggregateError } = require('sequelize');

async function InsertImageProcedure(idOrder, idproduct, sort, title, photourl, tumburl) {
  try{
    const result = await db.query(`
      EXEC orderprocess.s_step2_OrderDetail_ImageUpload_insertupdate 
        @idorder    = :param1,
        @idproduct  = :param2,
        @sort       = :param3,
        @title      = :param4,
        @photourl   = :param5,
        @tumburl    = :param6
    `, {
      replacements: { 
        param1: idOrder,
        param2: idproduct,
        param3: sort,
        param4: title,
        param5: photourl,
        param6: tumburl,
      },
      type: QueryTypes.RAW,
    });

    console.log("RESULT: ", result)

    return result[0];
  }catch(error){
    if (error instanceof AggregateError) {
      // Handle multiple errors
      const errors = error.errors;
      for (const err of errors) {
        console.error('Error:', err);
      }
    } else {
      // Handle other types of errors
      console.error('Error:', error);
    }
    
    return null;
  }
}

module.exports = InsertImageProcedure
