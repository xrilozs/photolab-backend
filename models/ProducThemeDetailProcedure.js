const db = require('../sequelize');
const { QueryTypes, AggregateError } = require('sequelize');

async function ProducThemeDetailProcedure(idproduct) {
  try{
    const result = await db.query(`
      EXEC store.s_ProductTheme_detail_view 
        @idproduct  = :param1
    `, {
      replacements: { 
        param1: idproduct,
      },
      type: QueryTypes.RAW,
    });

    console.log("RESULT: ", result)

    return result[0];
  }catch(error){
    if (error instanceof AggregateError) {
      // Handle multiple errors
      const errors = error.errors;
      for (const err of errors) {
        console.error('Error:', err);
      }
    } else {
      // Handle other types of errors
      console.error('Error:', error);
    }
    
    return null;
  }
}

module.exports = ProducThemeDetailProcedure
