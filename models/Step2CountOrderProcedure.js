const db = require('../sequelize');
const { QueryTypes, AggregateError } = require('sequelize');

async function Step2CountOrderProcedure(idOrder) {
  try{
    const result = await db.query(`
      SELECT [upload].[f_step2_order_countOrder](:idOrder) AS count
    `, {
      replacements: { idOrder: idOrder },
      type: QueryTypes.RAW,
    });

    return result && result[0].length > 0 ? result[0][0] : null;
  }catch(error){
    if (error instanceof AggregateError) {
      // Handle multiple errors
      const errors = error.errors;
      for (const err of errors) {
        console.error('Error:', err);
      }
    } else {
      // Handle other types of errors
      console.error('Error:', error);
    }
    
    return null
  }
}

module.exports = Step2CountOrderProcedure;
