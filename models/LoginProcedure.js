const db = require('../sequelize');
const { QueryTypes, AggregateError } = require('sequelize');

async function LoginProcedureExec(idOrder, phoneNo) {
  try{
    const result = await db.query(`
      DECLARE @result varchar(100);
      DECLARE @isPhotocomplete varchar(1);
      DECLARE @isUPloadDesignComplete varchar(1);
      DECLARE @isApprovalComplete varchar(1);
      DECLARE @latestStatus varchar(100);
      DECLARE @FullName varchar(100);
      DECLARE @orderdate date;
      DECLARE @expireddate date;
      DECLARE @dayexpired int;
      DECLARE @countorder int;
      EXEC orderprocess.s_step1_login 
        @language = 'ID',
        @IDOrder = :param2,
        @PhoneNo = :param3,
        @result = @result OUTPUT,
        @isPhotocomplete = @isPhotocomplete OUTPUT,
        @isUPloadDesignComplete = @isUPloadDesignComplete OUTPUT,
        @isApprovalComplete = @isApprovalComplete OUTPUT,
        @latestStatus = @latestStatus OUTPUT,
        @FullName = @FullName OUTPUT,
        @orderdate = @orderdate OUTPUT,
        @expireddate = @expireddate OUTPUT,
        @dayexpired = @dayexpired OUTPUT,
        @countorder = @countorder OUTPUT;
    `, {
      replacements: { param2: idOrder, param3: phoneNo },
      type: QueryTypes.RAW,
    });
    console.log("RESULT: ", result[0])

    return result[0] && result[0].length > 0 ? result[0][0] : null;
  }catch(error){
    if (error instanceof AggregateError) {
      // Handle multiple errors
      const errors = error.errors;
      for (const err of errors) {
        console.error('Error:', err);
      }
    } else {
      // Handle other types of errors
      console.error('Error:', error);
    }
    
    return null
  }
}

module.exports = LoginProcedureExec;
