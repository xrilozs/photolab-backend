const db = require('../sequelize');
const { QueryTypes, AggregateError } = require('sequelize');

async function EditTitleTemplateProcedure(idOrder, idproduct, sort, title) {
  try{
    const result = await db.query(`
      EXEC orderprocess.s_Step2_Orderdetail_Titletemplate 
        @idOrder    = :param1,
        @idproduct  = :param2,
        @sort       = :param3,
        @title      = :param4,
        @Theme      = null
    `, {
      replacements: { 
        param1: idOrder,
        param2: idproduct,
        param3: sort,
        param4: title,
      },
      type: QueryTypes.RAW,
    });

    console.log("RESULT: ", result)

    return true;
  }catch(error){
    if (error instanceof AggregateError) {
      // Handle multiple errors
      const errors = error.errors;
      for (const err of errors) {
        console.error('Error:', err);
      }
    } else {
      // Handle other types of errors
      console.error('Error:', error);
    }
    
    return false;
  }
}

module.exports = {
  EditTitleTemplateProcedure
}
