const db = require('../sequelize');
const { QueryTypes, AggregateError } = require('sequelize');

async function Step2OrderProcedure(idOrder) {
  try{
    const result = await db.query(`
      EXEC orderprocess.s_step2_order 
        @idorder = :param1
    `, {
      replacements: { param1: idOrder },
      type: QueryTypes.RAW,
    });

    return result[0];
  }catch(error){
    if (error instanceof AggregateError) {
      // Handle multiple errors
      const errors = error.errors;
      for (const err of errors) {
        console.error('Error:', err);
      }
    } else {
      // Handle other types of errors
      console.error('Error:', error);
    }
    
    return null
  }
}

module.exports = Step2OrderProcedure;
