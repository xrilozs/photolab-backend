const db = require('../sequelize');
const { QueryTypes, AggregateError } = require('sequelize');

async function AttributeProcedure() {
  try{
    const result = await db.query(`
      EXEC dbo.s_attribute 
        @attMaster = 'Upload',
        @attdetail = 'Everybook has',
        @language = 'ID'
    `, {
      type: QueryTypes.RAW,
    });

    return result[0];
  }catch(error){
    if (error instanceof AggregateError) {
      // Handle multiple errors
      const errors = error.errors;
      for (const err of errors) {
        console.error('Error:', err);
      }
    } else {
      // Handle other types of errors
      console.error('Error:', error);
    }
    
    return null
  }
}

module.exports = AttributeProcedure;
